# README #

### What is VIPER ###

VIPER is an application of Clean Architecture to iOS apps. The word VIPER is a backronym for View, Interactor, Presenter, Entity, and Routing.
Clean Architecture divides an app’s logical structure into distinct layers of responsibility.

### VIPER In detail ###

https://www.objc.io/images/issue-13/2014-06-07-viper-wireframe-76305b6d.png

### How to use? ###

* follow https://www.objc.io/issues/13-architecture/viper/
* Configuration
* Dependencies
