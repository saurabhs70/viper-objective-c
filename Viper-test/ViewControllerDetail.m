//
//  ViewControllerDetail.m
//  Viper-test
//
//  Created by Saurabh-pc on 3/16/17.
//  Copyright © 2017 test. All rights reserved.
//

#import "ViewControllerDetail.h"

@interface ViewControllerDetail ()

@end

@implementation ViewControllerDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setleft];
}
-(void)leftButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)setleft
{
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(leftButtonPressed)];
    // [leftBtns addObject:leftBtn];
    
    [self.navigationItem setLeftBarButtonItem:leftBtn animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
