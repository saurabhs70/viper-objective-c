//
//  ViewController.h
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CounterView.h"
#import "CounterPresenter.h"
#import "CounterDetailWireframe.h"
//#import "CounterDetailModule.h"
@interface ViewController : UIViewController<Counterview>
@property(weak,nonatomic)CounterPresenter *presenter;
//@property(weak,nonatomic)id<CounterListModuleCounterDetail>moduledetailcounter;
@property (weak, nonatomic) IBOutlet UILabel *lbl;

- (IBAction)btnPlusClicked:(id)sender;

- (IBAction)btnMinuClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tbl;

@end

