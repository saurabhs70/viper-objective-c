//
//  ViewController.m
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
-(void)Updatelabel:(int)lblvalue
{
    _lbl.text=[NSString stringWithFormat:@"%d",lblvalue];
}

- (IBAction)btnPlusClicked:(id)sender {
    [self.presenter Requestytoincrease];
}

- (IBAction)btnMinuClicked:(id)sender {
    [self.presenter Requestytodecrease];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
  
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Viewcontrollercell"];
    
    
    if (cell == nil) {
        
        /*
         *   Actually create a new cell (with an identifier so that it can be dequeued).
         */
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Viewcontrollercell"];
        
      //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    /*
     *   Now that we have a cell we can configure it to display the data corresponding to
     *   this row/section
     */
    
    
    cell.textLabel.text = @"ffffr";
   
    
    /* Now that the cell is configured we return it to the table view so that it can display it */
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //CounterDetailWireframe *vv=[[CounterDetailWireframe alloc]init];
   // [vv addViewController];
    
    [self.presenter addcontroller];
}
@end
