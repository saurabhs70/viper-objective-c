//
//  CounterDetailWireframe.m
//  Viper-test
//
//  Created by Saurabh-pc on 3/16/17.
//  Copyright © 2017 test. All rights reserved.
//

#import "CounterDetailWireframe.h"
#import "ViewControllerDetail.h"
@interface ViewControllerDetail()
{
    
}
@end
@implementation CounterDetailWireframe

- (void)addViewController
{
    
    UIStoryboard *storyboard = [self mainStoryboard];
ViewControllerDetail *detailvc = (ViewControllerDetail *)[storyboard instantiateViewControllerWithIdentifier:@"DetailvcId"];
    [APP_DELEGATE.navigationController pushViewController:detailvc animated:YES];
}


- (UIStoryboard *)mainStoryboard
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:[NSBundle mainBundle]];
    
    return storyboard;
}
-(void)presentAddInterfaceFromViewController
{
    [self addViewController];
}
@end
