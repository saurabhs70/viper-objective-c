//
//  CounterEntity.m
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import "CounterEntity.h"
@interface CounterEntity ()
{
    int counter;
}
@end
@implementation CounterEntity
-(void)increase

{
    counter++;
}
-(void)decrease
{
    counter--;
}
-(int)returncounter
{
    return counter;
}
@end
