//
//  CounterEntity.h
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CounterEntity : NSObject
-(void)increase;
-(void)decrease;
-(int)returncounter;
@end
