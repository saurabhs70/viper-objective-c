//
//  CounterIntractor.m
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import "CounterIntractor.h"

@implementation CounterIntractor
-(void)increase
{
    [self.entity increase];
      [self update];
}
-(void)decrese
{
    [self.entity decrease];
    [self update];
}
-(void)update
{
    int val=[self.entity returncounter];
    [self.presenter updatelbl:val];
}
@end
