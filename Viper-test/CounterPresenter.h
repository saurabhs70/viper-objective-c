//
//  CounterPresenter.h
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CounterIntractorIO.h"
#import "CounterView.h"
#import "CounterWireframe.h"
//#import "CounterDetailModule.h"
@interface CounterPresenter : NSObject<CounterIntractorOutput>
@property(weak,nonatomic) id <CounterIntractorInput>intractor;
@property(weak,nonatomic)id<Counterview>viewupdate;
@property (strong,nonatomic)CounterWireframe *counterwireframe;
-(void)Requestytoincrease;
-(void)Requestytodecrease;
-(void)addcontroller;
@end
