//
//  CounterIntractorIO.h
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CounterIntractorInput<NSObject>
-(void)increase;
-(void)decrese;
@end

@protocol CounterIntractorOutput<NSObject>
-(void)updatelbl:(int)lblvale;
@end
