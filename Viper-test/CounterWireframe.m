//
//  CounterWireframe.m
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import "CounterWireframe.h"
#import "CounterView.h"
#import "CounterPresenter.h"
#import "CounterEntity.h"
#import "CounterIntractor.h"
#import "ViewController.h"
#import "CounterDetailWireframe.h"
@interface CounterWireframe()
{
    CounterPresenter *presenter;
    CounterEntity *entity;
    CounterIntractor *intractor;
    CounterWireframe *wireframe;
    CounterDetailWireframe *wireframedetail;
}
@end
@implementation CounterWireframe
-(UIViewController*)updatecontroller:(UIViewController*)vc
{
    ViewController *viewc=(ViewController *)vc;
    presenter=[[CounterPresenter alloc]init];
    entity=[[CounterEntity alloc]init];
    intractor=[[CounterIntractor alloc]init];
    wireframe=[[CounterWireframe alloc]init];
    wireframedetail=[[CounterDetailWireframe alloc]init];
    
    presenter.intractor=intractor;
    intractor.presenter=presenter;
    intractor.entity=entity;
    viewc.presenter=presenter;
    presenter.viewupdate=viewc;
    
    wireframe.detailwireframe=wireframedetail;
    presenter.counterwireframe=wireframe;
    
    return viewc;
    
}
-(void)Presentdetalcontroller
{
    [self.detailwireframe presentAddInterfaceFromViewController];
}
@end
