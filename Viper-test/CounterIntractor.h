//
//  CounterIntractor.h
//  Viper-test
//
//  Created by Saurabh-pc on 3/15/17.
//  Copyright © 2017 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CounterIntractorIO.h"
#import "CounterEntity.h"
#import "CounterPresenter.h"
@interface CounterIntractor : NSObject<CounterIntractorInput>
@property(nonatomic,weak)CounterEntity *entity;
@property (weak,nonatomic)id <CounterIntractorOutput> presenter;


@end
